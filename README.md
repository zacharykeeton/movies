# movies

nightly web scraping of my favorite theatres and a react UI.  fully containerized for docker stack

How it works:

A scraper container is on a 2:30am each day cron job where it scrapes the websites of various movie theatres and persists the data in to a mongodb container.

On the other side, there is an graphql api container that connects to that same mongo container and serves data to the ui container (which is the NGINX container).  The ui files are served statically by NGINX and the api requests are proxy passed through NGINX to the api server.

As of May 20, 2019, you can see it live at http://zacharykeeton.com/apps/movies/

## Development

To run in a dev environment, start a mongo container on localhost:27017.
```bash
docker run -p27017:27017 -d --name dev-mongo mongo
```

Then, you can run the scraper at will with `npm run start-dev`.

Start the api in dev mode too with `npm run start-dev`.

You run the ui though with just `npm start` (it knows it's in dev mode).


## Build & Push
Once you are ready to deploy, you can go to the top level directory and do `npm run build-docker-push-all` (make sure you are on the correct branch). This will build all images and push to docker hub at `:latest`.

## Deploy
To deploy, build, push (previous step) and scp the top-level `docker-stack.yml` file to your server running docker (in swarm mode).  Then, ssh into your server and just do another `docker stack deploy -c docker-stack.yml movies`

NOTE: The project is currently geared you run behind another master reverse proxy server.  If you want to deploy it standalone, you'll need to set the listening port on the nginx service (in the stack file) also...

NOTE 2: You'll need to adjuse the `homepage` value in `./ui/package.json`. Likely you'll just need to strip off the path.