/**
 * Read in schema file as string and pass to buildSchema().
 * Avoids giant, magic schema string.
 * Also allows for intellisense. See schema.gql
 */

const fs = require('fs');
const { join } = require('path');
const { buildSchema } = require('graphql');


module.exports = buildSchema(
  fs.readFileSync(join(__dirname, 'schema.gql'), 'utf-8')
);