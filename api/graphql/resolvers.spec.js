const { timeAsInt } = require('./resolvers');
const assert = require('assert');

describe('timeAsInt', function () {

    describe(`should return the corresponding time int between 1 and 2400 given a date object.`, function () {

        const testCases = [
            // MORNING
            // year, monthIndex, day, hour(0-23), minutes, seconds
            //                   y  m  d  h  m   s
            { input: new Date(2019, 5, 6, 6, 4, 58), expectedResult: 604 }, 
            { input: new Date(2019, 5, 6, 0, 30, 58), expectedResult: 30 },
            { input: new Date(2019, 5, 6, 14, 56, 58), expectedResult: 1456 },
        ];

        for(let testCase of testCases) {
            it(`should return ${testCase.expectedResult} given the date input "${testCase.input}".`, function () {
                assert.equal(timeAsInt(testCase.input), testCase.expectedResult);
            });
        }

    });
});