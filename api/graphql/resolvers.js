const { getDb } = require('../mongoUtil');

// a generic, paged, fetcher
const fetch = async (collectionName, pageSize, pageNum, query = {}) => {
	const db = getDb();
	return await db
		.collection(collectionName)
		.find(query)
		.sort({ name: 1 })
		.limit(pageSize)
		.skip((pageNum - 1) * pageSize)
		.toArray();
};

// helper func to turn current time to 24-hour int.
// e.g.  2:45pm -> 1445
const timeAsInt = (date) => {
	const hour = date.getHours();
	const minutes = date.getMinutes();
	const timeInt = hour * 100 + minutes;
	return timeInt;
};

// Fetch certain 'showing' documents given an array of selected theatre names.
// Also, if 'hidePastShowings' is true, only fetch those showings with showtimes >= now.
const fetchSelectedTheatreShowings = async ({ selectedTheatres, pageSize, pageNum, hidePastShowings }) => {
	const showingsQuery = { theatreName: { $in: selectedTheatres } };

	if (hidePastShowings) {
		showingsQuery.showtime = { $gte: timeAsInt(new Date()) };
	}

	return await fetch('showings', pageSize, pageNum, showingsQuery);
};

/**
 * The api call performed when UI page (app) is first loading.
 * Loads theatres, errors, metadata, etc.
 * If there are already some selected theatres (i.e. user has been here before),
 * we load the first page of showings for those theatres.
 * If selectedTheatres is null (i.e. this is the user's first visit), we fetch
 * showing records from ALL theatres.
 * Business rule: on first visit, all theatres should be selected.
 */
const fetchInitialLoad = async ({ pageSize, selectedTheatres, hidePastShowings }) => {
	// Fetch showings in theaters list, or any showings if no theatres list (first visit).
	const showingsQuery = selectedTheatres ? { theatreName: { $in: selectedTheatres } } : {};

	// Respect 'hidePastShowings' and only fetch showings with showtime >= now
	if (hidePastShowings) {
		showingsQuery.showtime = { $gte: timeAsInt(new Date()) };
	}

	const db = getDb();
	const theatresCollection = db.collection('theatres');
	const collectionErrorsCollection = db.collection('collectionErrors');
	const metaCollection = db.collection('metas');
	const showingsCollection = db.collection('showings');

	let theatres, showings, collectionErrors, meta, totalShowings;

	// Perform all the queries asynchronously. Wait until all are finished.
	await Promise.all([
		theatresCollection
			.find({})
			.toArray()
			.then(result => (theatres = result)),
		showingsCollection
			.find()
			.count()
			.then(result => (totalShowings = result)),
		showingsCollection
			.find(showingsQuery)
			.limit(pageSize)
			.toArray()
			.then(result => (showings = result)),
		collectionErrorsCollection
			.find({})
			.toArray()
			.then(result => (collectionErrors = result)),
		metaCollection.findOne({}).then(result => (meta = result)),
	]);

	return {
		theatres,
		showings,
		collectionErrors,
		meta,
		totalShowings,
	};
};

module.exports = {
	fetchSelectedTheatreShowings,
	fetchInitialLoad,
	timeAsInt // exported for unit testing.
};
