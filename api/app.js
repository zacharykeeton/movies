/**
 * API PROCESS ENTRYPOINT
 * Configure and start api server with a graphql endpoint.
 * See package.json->scripts->start
 */

const express = require("express");
const cors = require("cors");
const expressGraphql = require("express-graphql");

const { connectToServer, DB_URL } = require("./mongoUtil");
const graphqlSchema = require("./graphql/schema");
const graphqlResolver = require("./graphql/resolvers");

const app = express();

/** CONFIGURE CORS
 * Whitelist predicated upon NODE_ENV.
 * See package.json->scripts->start-dev.
 * Default to production values.
 */
const whitelist =
  process.env.NODE_ENV === "development"
    ? ["http://localhost:3001", "http://192.168.1.63:3001"]
    : ["http://192.168.99.101", "http://34.70.250.240"]; // production

const corsOptions = {
  origin: function(origin, callback) {
    // !origin: allow for container to container comms
    if (whitelist.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error(`Not allowed by CORS: ${origin}`));
    }
  }
};

app.use(cors(corsOptions));
//// END CORS CONFIG

const main = async () => {
  try {
    await connectToServer();
  } catch (err) {
    // Will ususally happen on stack deploy when mongo container isn't up yet.
    console.error(`ERROR: Could not connect to db: ${DB_URL}. Exiting.`);
    process.exitCode = 1;
    return;
  }

  app.post(
    "/graphql",
    expressGraphql({
      schema: graphqlSchema,
      rootValue: graphqlResolver
    })
  );

  app.listen(3000);
  console.log("Movies API listening at: 3000");
};

main();
