/**
 * Utility module to share a single connection reference
 * accross other modules.
 */

const MongoClient = require("mongodb").MongoClient;

let DB_URL = "mongodb://mongo-cluster-ip-service:27017";
// process.env.NODE_ENV === "development"
//   ? "mongodb://localhost:27017"
//   : ;

// allows for mongo connection URL override via
// environment variable.
if (process.env.MOVIES_DB_URL) {
  DB_URL = process.env.MOVIES_DB_URL;
}

let _db;

module.exports = {
  connectToServer: async () => {
    const client = await MongoClient.connect(DB_URL, { useNewUrlParser: true });
    _db = client.db("movies");
  },

  getDb: function() {
    return _db;
  },

  DB_URL
};
