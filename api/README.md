# API

The api runs in a docker container and exposes a grapql api endpoint. See `./graphql/schema.gql` for details. It fetches data from the mongo container (that needs to be up before the api will run).

## Development

To run in a dev environment, start a mongo container on localhost:27017.
```bash
docker run -p27017:27017 -d --name dev-mongo mongo
```

Then, you can run the api with `npm run start-dev`.

## Build & Push
Once you are ready to deploy, you can go to `./api/` and do `npm run build-docker-push` (make sure you are on the correct branch). This will build the api image and push it to docker hub at `:latest`. See `package.json` for details.

### Example query requests:

#### Fetch Initial Load (posted on app mount)
```bash
curl -X POST \
  http://127.0.0.1:3000/graphql \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "query": "query FetchInitialLoad($pageSize: Int = 10, $selectedTheatres: [String], $hidePastShowings: Boolean = false) {fetchInitialLoad(pageSize: $pageSize, selectedTheatres: $selectedTheatres, hidePastShowings: $hidePastShowings) { meta{ collectedAt } showings{ movieTitle showtime theatreName moviePosterUrl movieDetailsUrl } totalShowings collectionErrors{message} theatres{ name } }}",
    "variables": {
        "selectedTheatres": ["Santikos CasaBlanca"],
        "pageSize": 1000,
        "hidePastShowings": false
    }
}'
```

#### Fetch Showings from Selected Theatres (posted on settingsList unmount)
```bash
curl -X POST \
  http://127.0.0.1:3000/graphql \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "query": "query FetchSelectedTheatreShowings($selectedTheatres: [String]!, $pageSize: Int = 10, $pageNum: Int = 1) {fetchSelectedTheatreShowings(selectedTheatres: $selectedTheatres, pageSize: $pageSize, pageNum: $pageNum) { showtime movieTitle theatreName moviePosterUrl movieDetailsUrl } }",
    "variables": {
    	"selectedTheatres":["Drafthouse - Westlakes", "Santikos Palladium"],
        "pageSize": 100,
        "pageNum": 1
    }
}'
```

