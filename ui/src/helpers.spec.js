import { formatTimeFromInt } from './helpers';

describe('formatTimeFromInt', function () {

    describe(`should return the correct 12-hour time string given an integer representation of time (0-2400).`, function () {

        const testCases = [
            
            { input: 0, expectedResult: '12:00 a.m.' }, 
            { input: 1045, expectedResult: '10:45 a.m.' },
            { input: 1200, expectedResult: '12:00 p.m.' },
            { input: 1230, expectedResult: '12:30 p.m.' },
            { input: 1245, expectedResult: '12:45 p.m.' },
            { input: 1445, expectedResult: '2:45 p.m.' },
            { input: 1500, expectedResult: '3:00 p.m.' },
            { input: 2400, expectedResult: '12:00 a.m.' },
            { input: 2430, expectedResult: '12:30 a.m.' },
            { input: 2459, expectedResult: '12:59 a.m.' },

        ];

        for(let testCase of testCases) {
            it(`should return the string "${testCase.expectedResult}" given the integer "${testCase.input}".`, function () {
                expect(formatTimeFromInt(testCase.input)).toEqual(testCase.expectedResult);
            });
        }

    });

    describe(`should return the correct 24-hour time string given an integer representation of time (0-2400).`, function () {

        const testCases = [
            
            { input: 0, expectedResult: '24:00' }, 
            { input: 1045, expectedResult: '10:45' },
            { input: 1200, expectedResult: '12:00' },
            { input: 1230, expectedResult: '12:30' },
            { input: 1245, expectedResult: '12:45' },
            { input: 1445, expectedResult: '14:45' },
            { input: 1500, expectedResult: '15:00' },
            { input: 2400, expectedResult: '24:00' },
            { input: 2430, expectedResult: '24:30' },
            { input: 2459, expectedResult: '24:59' },

        ];

        for(let testCase of testCases) {
            it(`should return the string "${testCase.expectedResult}" given the integer "${testCase.input}".`, function () {
                expect(formatTimeFromInt(testCase.input, true)).toEqual(testCase.expectedResult);
            });
        }

    });

    describe(`should throw if the given time integer is out-of-bounds (not 0-2459).`, function () {

        const testCases = [
            { input: -1 }, 
            { input: 2500 }, 
            { input: null }, 
            { input: undefined }, 
            { input: "zach" }
        ];

        for(let testCase of testCases) {
            it(`should throw if the time integer is "${testCase.input}"`, function () {
                expect(() => formatTimeFromInt(testCase.input)).toThrow();
            });
        }

    });
});