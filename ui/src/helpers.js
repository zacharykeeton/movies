// convert time ints into readable time for UI display.
export function formatTimeFromInt(timeAsInt, return24HourTime = false) {
    // e.g. 2300 => "11:00pm"
    // e.g. 2300 => "23:00"

    timeAsInt = parseInt(timeAsInt);

    if(isNaN(timeAsInt) || timeAsInt < 0 || timeAsInt > 2459) {
        throw new Error(`Input must parse to an integer between 0 and 2459. Got ${timeAsInt}`)
    }

    const hour = Math.floor(timeAsInt/100);
    const minute = timeAsInt % 100;

    const minuteString = ("0" + minute).slice(-2);

    if(return24HourTime)
    {
        if(timeAsInt === 0) {
            return '24:00';
        }
        return `${hour}:${minuteString}`; 
    } 
    else {

        const AM = 'a.m.';
        const PM = 'p.m.';

        // handle midnight hour
        if(hour === 0 || hour === 24){
            return `12:${minuteString} ${AM}`;
        }

        // handle noon hour
        else if(hour === 12){
            return `12:${minuteString} ${PM}`;
        }

        // handle morning
        else if(hour < 12) {
            return `${hour}:${minuteString} ${AM}`;
        }

        // handle afternoon/evening
        else {
            return `${hour-12}:${minuteString} ${PM}`;
        }
    }    
}