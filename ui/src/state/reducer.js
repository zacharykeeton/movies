/**
 * Simple redux reducers.
 */

import { combineReducers } from 'redux';

// REDUCERS

const theatresReducer = (state = [], action) => {
	if (action.type === 'LOAD_THEATRES') {
		return [...state, ...action.payload];
	}
	return state;
};

const showingsReducer = (state = [], action) => {
	if (action.type === 'LOAD_SHOWINGS') {
		return action.payload;
	}
	return state;
};

const errorsReducer = (state = [], action) => {
	if (action.type === 'LOAD_ERRORS') {
		return [...state, ...action.payload];
	}
	return state;
};

const collectedAtReducer = (state = null, action) => {
	if (action.type === 'LOAD_COLLECTED_AT') {
		return action.payload;
	}
	return state;
};

const timeFormatSettingReducer = (state = false, action) => {
	if (action.type === 'TOGGLE_TIME_FORMAT') {
		return !state;
	}
	return state;
};

const hidePastShowingsSettingReducer = (state = true, action) => {
	if (action.type === 'TOGGLE_HIDE_PAST_SHOWINGS') {
		return !state;
	}
	return state;
};

const darkModeSettingReducer = (state = true, action) => {
	if (action.type === 'TOGGLE_DARK_MODE') {
		return !state;
	}
	return state;
};

const selectedTheatresReducer = (state = [], action) => {
	if (action.type === 'TOGGLE_THEATRE_SELECTED') {
		const selectedTheatres = new Set(state); // convert to set to clear any dupes.

		const theatreName = action.payload;

        selectedTheatres.has(theatreName) 
            ? selectedTheatres.delete(theatreName) 
            : selectedTheatres.add(theatreName);

		return Array.from(selectedTheatres); // return new array (serializable).
	} else if (action.type === 'LOAD_SELECTED_THEATRES_FROM_LIST') { // on first visit
		return [...new Set(action.payload)];
	}

	return state;
};

const firstVisitReducer = (state = false, action) => {
	if (action.type === 'LOAD_SELECTED_THEATRES_FROM_LIST') {
		return false;
	}
	return state;
};

const loadingReducer = (state = false, action) => {
	if (action.type === 'LOADING_BEGIN') {
		return true;
	}

	if (action.type === 'LOADING_END') {
		return false;
	}

	return state;
};

const rootReducer = combineReducers({
	collectedAt: collectedAtReducer,
	theatres: theatresReducer,
	showings: showingsReducer,
	errors: errorsReducer,
	use24HourTimeFormat: timeFormatSettingReducer,
	hidePastShowings: hidePastShowingsSettingReducer,
	selectedTheatres: selectedTheatresReducer,
	darkMode: darkModeSettingReducer,
	firstVisit: firstVisitReducer,
	loading: loadingReducer,
});

export default rootReducer;
