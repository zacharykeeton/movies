/**
 * Manage localStorage ops.
 */

export const loadStateFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if(serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } 
    catch (err) {  // if user disallowed local storage in settings. 
        return undefined;        
    }
}

export const saveStateToLocalStorage = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } 
    catch (err) { // if user disallowed local storage in settings.
        // do nothing or log        
    }
}