//// THUNKS

let API_BASE_URL = `${process.env.PUBLIC_URL}/api`;

// npm run build will strip this out (according to docs)
// so leave it written like this.
if (process.env.NODE_ENV !== "production") {
  API_BASE_URL = "http://localhost:3000";
}

// the initial data load when arriving on page. Plus, the
// first page of the showings.

const fetchGraphQl = ({ query, variables }) => {
  return fetch(API_BASE_URL + "/graphql", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "cache-control": "no-cache",
      pragma: "no-cache"
    },
    method: "POST",
    body: JSON.stringify({
      query,
      variables
    })
  }).then(function(response) {
    return response.json();
  });
};
export const fetchData = () => (dispatch, getState) => {
  dispatch({ type: "LOADING_BEGIN" });

  const {
    firstVisit: isFirstVisit,
    hidePastShowings,
    selectedTheatres: previouslySelectedTheatres
  } = getState();
  const selectedTheatres = isFirstVisit ? null : previouslySelectedTheatres;

  const query = `query FetchInitialLoad($pageSize: Int = 10, $selectedTheatres: [String], $hidePastShowings: Boolean = false) {
						fetchInitialLoad(pageSize: $pageSize, selectedTheatres: $selectedTheatres, hidePastShowings: $hidePastShowings) { 
							meta{ collectedAt } 
							showings{ movieTitle showtime theatreName moviePosterUrl movieDetailsUrl } 
							totalShowings 
							collectionErrors{message} 
							theatres{ name } 
						}
					}`;

  const variables = {
    selectedTheatres,
    pageSize: 1000,
    hidePastShowings
  };

  fetchGraphQl({ query, variables }).then(result => {
    const {
      meta,
      showings,
      collectionErrors,
      theatres
    } = result.data.fetchInitialLoad;

    dispatch({ type: "LOAD_THEATRES", payload: theatres });
    dispatch({ type: "LOAD_ERRORS", payload: collectionErrors });
    dispatch({
      type: "LOAD_COLLECTED_AT",
      payload: meta ? meta.collectedAt : null
    });
    dispatch({ type: "LOAD_SHOWINGS", payload: showings });

    // first visit, auto select all theatres
    if (isFirstVisit) {
      dispatch({
        type: "LOAD_SELECTED_THEATRES_FROM_LIST",
        payload: getState().theatres.map(theatre => theatre.name)
      });
    }

    dispatch({ type: "LOADING_END" });
  });
  // .catch(result => {
  //   dispatch({ type: "LOADING_END" });
  //   dispatch({ type: "LOAD_ERRORS", payload: [result.message] });
  //   console.error("zzz", result);
  // });
};

// only fetches showings from selected theatres. Used when leaving settings screen.
export const fetchSelectedTheatreShowings = selectedTheatres => (
  dispatch,
  getState
) => {
  dispatch({ type: "LOADING_BEGIN" });

  const hidePastShowings = getState().hidePastShowings;

  const variables = {
    selectedTheatres: getState().selectedTheatres,
    pageSize: 1000,
    pageNum: 1,
    hidePastShowings
  };

  const query = `query FetchSelectedTheatreShowings($selectedTheatres: [String]!, $pageSize: Int = 10, $pageNum: Int = 1, $hidePastShowings: Boolean = false) {
                        fetchSelectedTheatreShowings(selectedTheatres: $selectedTheatres, pageSize: $pageSize, pageNum: $pageNum, hidePastShowings: $hidePastShowings) { 
                            showtime 
                            movieTitle 
                            theatreName 
                            moviePosterUrl 
                            movieDetailsUrl 
                        } 
					}`;

  fetchGraphQl({ query, variables }).then(result => {
    dispatch({
      type: "LOAD_SHOWINGS",
      payload: result.data.fetchSelectedTheatreShowings
    });
    dispatch({ type: "LOADING_END" });
  });
};

//// ACTION CREATORS

export const toggleTheatreSelected = theatreName => {
  return {
    type: "TOGGLE_THEATRE_SELECTED",
    payload: theatreName
  };
};

export const toggleTimeFormatSetting = () => {
  return {
    type: "TOGGLE_TIME_FORMAT"
  };
};

export const toggleHidePastShowings = () => {
  return {
    type: "TOGGLE_HIDE_PAST_SHOWINGS"
  };
};

export const toggleDarkMode = () => {
  return {
    type: "TOGGLE_DARK_MODE"
  };
};
