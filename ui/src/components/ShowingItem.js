/**
 * An indivual 'movie showing' entry.
 */


import React from 'react';
import { formatTimeFromInt } from '../helpers';
import { connect } from 'react-redux';
import './ShowingItem.css';

const ShowingItem = ({showing, use24HourTimeFormat}) => {

    return (
        <a className="movie-item" href={showing.movieDetailsUrl}>
            <div className="movie-item__time">
                {formatTimeFromInt(showing.showtime, use24HourTimeFormat)}
            </div>
            <div className="movie-item__info">
                <div className="movie-item__title">
                    {showing.movieTitle}
                </div>
                <div className="movie-item__description">
                    {showing.theatreName}
                </div>
            </div>
            <div className="movie-item__poster">
                <img className="movie-item__image" src={showing.moviePosterUrl} alt={`${showing.movieTitle} poster`} />
            </div>
        </a>
    );
};

const mapStateToProps = state => {
    return { 
        use24HourTimeFormat: state.use24HourTimeFormat
    };
}

export default connect(mapStateToProps)(ShowingItem);