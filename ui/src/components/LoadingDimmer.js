/**
 * A simple loading dimmer using when fetching from the api.
 */

import React from 'react';
import { connect } from 'react-redux';

const LoadingDimmer = props => {
	let className = 'ui active dimmer';

	if (!props.darkMode) {
		className += ' inverted'; // to make into 'light-mode'
	}

	return (
		<div className={className}>
			<div className="ui text loader">Loading</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {
		darkMode: state.darkMode,
	};
};

export default connect(mapStateToProps)(LoadingDimmer);
