/**
 * Handles the setting screen.
 */

import React from 'react';
import HidePastShowings from './HidePastShowingsSetting';
import DarkModeSetting from './DarkModeSetting';
import TimeFormatSetting from './TimeFormatSetting';
import TheatresList from './TheatresList';
import { connect } from 'react-redux';
import { fetchSelectedTheatreShowings } from '../state/actions';

class SettingsList extends React.Component {
	constructor(props) {
		super(props);
		this.selectedTheatres = props.selectedTheatres;
		this.hidePastShowings = props.hidePastShowings;
	}

	componentWillUnmount() {
		const someTheatresSelected = this.props.selectedTheatres.length > 0;
		
		const selectedTheatresChanged = this.selectedTheatres !== this.props.selectedTheatres;
		const hidePastShowingsChanged = this.hidePastShowings !== this.props.hidePastShowings;

		if ((hidePastShowingsChanged || selectedTheatresChanged) && someTheatresSelected) {
			this.props.fetchSelectedTheatreShowings();
		}
	}

	render() {
		return (
			<div className="settings-list">
				<DarkModeSetting />
				<TimeFormatSetting />
				<HidePastShowings />
				<TheatresList />
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		selectedTheatres: state.selectedTheatres,
		hidePastShowings: state.hidePastShowings
	};
};
export default connect(
	mapStateToProps,
	{ fetchSelectedTheatreShowings }
)(SettingsList);
