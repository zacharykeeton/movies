/**
 * Shown when api call returned no showings (either scraping problem or all
 * showtimes are in the past and 'HidePastShowings' is true).
 */

import React from 'react';
import Prompt from './Prompt';

const NoShowsFoundPrompt = () => (
        <Prompt 
            mainText="No showtimes found."
            secondaryText="Either adjust settings or try again later."
            iconClassName="exclamation triangle icon"
            linkTo="/settings"
        />
);

export default NoShowsFoundPrompt;