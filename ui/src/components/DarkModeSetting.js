/**
 * The toggle element for DarkMode on the settings page.
 */

import React from 'react';
import { connect } from 'react-redux';
import { toggleDarkMode } from '../state/actions';
import ToggleSetting from './ToggleSetting';

// TIMEFORMAT
const DarkModeSettingComponent = ({darkMode, toggleDarkMode}) => {
    return <ToggleSetting 
                mainText="Dark mode." 
                secondaryText="Toggle between dark and light themes." 
                value={darkMode} 
                onClick={toggleDarkMode} 
            />
};

const mapStateToProps = state => ({ darkMode: state.darkMode });

export default connect(mapStateToProps, {toggleDarkMode})(DarkModeSettingComponent);
