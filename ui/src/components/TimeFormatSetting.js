/**
 * Toggle btw 12 and 24-hour time view on page.
 */

import React from 'react';
import { connect } from 'react-redux';
import { toggleTimeFormatSetting } from '../state/actions';
import ToggleSetting from './ToggleSetting';

// TIMEFORMAT
const TimeFormatSettingComponent = ({use24HourTimeFormat, toggleTimeFormatSetting}) => {
    return <ToggleSetting 
                mainText="Use 24-hour time format" 
                secondaryText="Toggle between 12 and 24 hour showtime displays." 
                value={use24HourTimeFormat} 
                onClick={toggleTimeFormatSetting} 
            />
};

const mapStateToProps_timeFormat = state => ({ use24HourTimeFormat: state.use24HourTimeFormat });

export default connect(mapStateToProps_timeFormat, {toggleTimeFormatSetting})(TimeFormatSettingComponent);