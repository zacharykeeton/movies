/**
 * Build list of movie theatres on settings page.
 */

import React from 'react';
import { connect } from 'react-redux';

import TheatreItem from './TheatreItem';

class TheatresList extends React.Component {
    render() {
        return (
            <React.Fragment>
                {
                    this.props.theatres.map((theatre, index) => (
                        <TheatreItem key={index} theatre={theatre} />
                    ))
                }
            </React.Fragment>
        );
    }
}

export default connect(state => ({ theatres: state.theatres }))(TheatresList);