/**
 * Generic component used in other files (e.g. SelectTheatresPrompt.js).
 */

import React from 'react';
import { Link } from 'react-router-dom';
import './Prompt.css';

const Prompt = (props) => (
    <Link to={props.linkTo}>
        <div className="prompt">
            <p className="prompt__main-text">{props.mainText}</p>
            <p className="prompt__secondary-text">{props.secondaryText}</p>
            <p className="prompt__icon"><i className={props.iconClassName}></i></p>
        </div>
    </Link>
);

export default Prompt;