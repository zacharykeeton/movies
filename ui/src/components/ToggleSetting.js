/**
 * Generic toggle setting used in:
 * TimeFormatSetting.js
 * HidePastShowingsSetting.js
 * etc.
 */

import React from 'react';

import './ToggleSetting.css';

class ToggleSetting extends React.Component {
    render() {

        const checkIcon = this.props.value ? <i className="check icon"></i> : null;

        return (
            <div className="toggle-setting" onClick={this.props.onClick}>
                <div className="toggle-setting__checkmark">
                    {checkIcon}
                </div>
                <div className="toggle-setting__info">
                    <div className="toggle-setting__main-text">
                        {this.props.mainText}
                    </div>
                    <div className="toggle-setting__secondary-text">
                        {this.props.secondaryText}
                    </div>
                </div>
            </div>
        );
    };
}

export default ToggleSetting;