/**
 * The toggle element for HidePastShowings on the settings page.
 */

import React from 'react';
import { connect } from 'react-redux';
import { toggleHidePastShowings } from '../state/actions';
import ToggleSetting from './ToggleSetting';

// TIMEFORMAT
const HidePastShowingsSettingComponent = ({ hidePastShowings, toggleHidePastShowings }) => {
	return (
		<ToggleSetting
			mainText="Hide past showings."
			secondaryText="Toggle between showing the showings for the entire day vs. now onwards."
			value={hidePastShowings}
			onClick={toggleHidePastShowings}
		/>
	);
};

const mapStateToProps_hidePastShowings = state => ({ hidePastShowings: state.hidePastShowings });

export default connect(
	mapStateToProps_hidePastShowings,
	{ toggleHidePastShowings }
)(HidePastShowingsSettingComponent);
