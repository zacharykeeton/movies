/**
 * Handles main display of 'movie showting' entries plus the prompts
 * when there are no showings, theatres selected, page is loading, etc.
 */

import ShowingItem from './ShowingItem';
import React from 'react';
import { connect } from 'react-redux';
import { fetchSelectedTheatreShowings } from '../state/actions';

import LoadingDimmer from './LoadingDimmer';
import SelectTheatresPrompt from './SelectTheatresPrompt';
import NoShowsFoundPrompt from './NoShowsFoundPrompt';

class ShowingsList extends React.Component {
    
	render() {
        
        const {loading, isFirstVisit, someTheatresSelected, showings } = this.props;

        if(loading) {
            return <LoadingDimmer />;
        }

        if(!isFirstVisit && !someTheatresSelected) {
            return <SelectTheatresPrompt />
        }
        // NOTE: If it IS the first visit, then ALL the theatres
        // should be selected.

        if(showings.length === 0) {
            return <NoShowsFoundPrompt />;
        }
        
		return (
			<div className="showings-list">{
                showings.map((showing, index) => {
                    return <ShowingItem key={index} showing={showing} />;
                })
            }</div>
        );
	}
}

const mapStateToProps = state => {
	return {
		showings: state.showings,
        someTheatresSelected: state.selectedTheatres.length > 0,
        isFirstVisit: state.firstVisit,
        loading: state.loading
	};
};

export default connect(mapStateToProps, {fetchSelectedTheatreShowings})(ShowingsList);
