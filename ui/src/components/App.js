import React from 'react';
import Menu from './Menu';
import ShowingsList from './ShowingsList';
import SettingsList from './SettingsList';

import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';

import { fetchData } from '../state/actions';

class App extends React.Component {

    componentDidMount() {
        // fetch theatres, errors, meta, and first page of movie showings
        this.props.fetchData();
    }
    
    render() {

        const theme = this.props.darkMode ? 'dark' : 'light';

        // set light darkmode.
        // basename is for deploying behind proxies.
        // PUBLIC_URL set in package.json->homepage
        return (
            <div className={`app app--${theme}`}>
                <BrowserRouter basename={process.env.PUBLIC_URL}>
                    <Menu />
                    <Route path="/" exact component={ShowingsList} />
                    <Route path="/settings" exact component={SettingsList} />
                </BrowserRouter>
            </div>
        );
    }
};

const mapStateToProps = state => ({ 
    someTheatresSelected: state.selectedTheatres.length > 0,
    darkMode: state.darkMode
});

export default connect(mapStateToProps, {fetchData})(App);