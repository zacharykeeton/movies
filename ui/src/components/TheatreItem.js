/**
 * Individual theatre entry on the settings page.
 */

import React from 'react';
import { connect } from 'react-redux';
import ToggleSetting from './ToggleSetting';
import { toggleTheatreSelected } from '../state/actions';

class TheatreItem extends React.Component {
    render() {

        const theatreName = this.props.theatre.name;
        const selected = this.props.selectedTheatres.includes(theatreName);

        return (
            <ToggleSetting 
                mainText={ theatreName }
                secondaryText="show/hide" 
                value={selected} 
                onClick={() => this.props.toggleTheatreSelected(theatreName)} 
            />
        );
    } 
}

export default connect(state => ({ selectedTheatres: state.selectedTheatres }), { toggleTheatreSelected })(TheatreItem);