/**
 * Handle the top, fixed nav menu.  Displays the scraping date.
 */

import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import "./Menu.css";

const Menu = props => {
  let formattedDate;
  if (props.collectedAt) {
    const options = {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric"
    };
    formattedDate = new Intl.DateTimeFormat("en-US", options).format(
      new Date(props.collectedAt)
    );
  }

  return (
    <div className="menu">
      <div className="menu-items">
        <Link
          to="/"
          className="menu-items__item menu-items__item--showtimes-link"
        >
          Showtimez
        </Link>
        <div className="menu-items__item menu-items__item--collection-date">
          for {formattedDate}
        </div>
        {/* 
                TODO: ERRORS ICON AND PAGE
                <Link to="/settings" className="menu-items__item menu-items__item--settings-link">
                    <i className="cog icon"></i>
                </Link>  */}
        <Link
          to="/settings"
          className="menu-items__item menu-items__item--settings-link"
        >
          <i className="cog icon"></i>
        </Link>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    collectedAt: state.collectedAt
  };
};
export default connect(mapStateToProps)(withRouter(Menu));
