/**
 * Screen shown when the user explicitly deselects 
 * all movie theatres and tries to view showings.
 */

import React from 'react';
import Prompt from './Prompt';

const SelectTheatresPrompt = () => (
        <Prompt 
            mainText="You have no theatres selected."
            secondaryText="Please go to settings and select your favorite theatres."
            iconClassName="cog icon"
            linkTo="/settings"
        />
);

export default SelectTheatresPrompt;