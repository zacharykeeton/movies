/**
 * The effective entrypoint of the UI.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import throttle from 'lodash.throttle';

import rootReducer from './state/reducer';
import App from './components/App';
import { loadStateFromLocalStorage, saveStateToLocalStorage } from './state/localStorage';

import './index.css';

// SETUP REDUX STORE
// Initialize with data pulled from localStorage, or if first visit (LS is null),
// initialize saying so.
const initialState = loadStateFromLocalStorage() || { firstVisit: true };
const store = createStore(rootReducer, initialState, applyMiddleware(ReduxThunk));

// SAVE USER SETTINGS TO LOCAL STORAGE WHEN STATE CHANGES
// NOTE: throttled at once per second.
store.subscribe(
	throttle(() => {
		const state = store.getState();
		saveStateToLocalStorage({
			use24HourTimeFormat: state.use24HourTimeFormat,
			hidePastShowings: state.hidePastShowings,
			selectedTheatres: state.selectedTheatres,
			darkMode: state.darkMode,
		});
	}, 1000)
);

// START UI APP
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
