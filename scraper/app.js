/**
 * Scraper entry point. See 'main()' below.
 * Essentially does 1) scrape all and 2) save to db
 * with some stdout progress statements.
 * BUSINESS RULE: Accumulate errors and save to db anyway
 * for push to ui.
 */

const { connectToDbIfNotConnected, saveToDb } = require('./data/persist');
const scrapeAll = require('./scrapeAll');
const ScrapeResults = require('./scrapers/ScrapeResults');

const scrape = async () => {
	console.log('STARTING SCRAPE', new Date().toLocaleString());
	console.time('scraping_duration');
	const results = await scrapeAll();
	console.log('SCRAPE COMPLETE', new Date().toLocaleString());
	console.timeEnd('scraping_duration');
	return results;
};

const persist = async scrapeResults => {
	await saveToDb(scrapeResults);
};

const main = async () => {
	console.time('total_duration');

	// Connect to db now so we can fail before scraping.
	try {
		await connectToDbIfNotConnected();
	} catch (error) {
		console.error(`ERROR: Could not connect to db. Exiting.`);
		process.exitCode = 1;
		return;
	}

	// Newed up here to accumulate top-level errors.
	// Will be replaced with actual scrape
	// results below.
	let scrapeResults = new ScrapeResults();

	try {
		scrapeResults = await scrape();
	} catch (err) {
		scrapeResults.errors.push({ message: 'SCRAPING ERROR ' + err.message });
		process.exitCode = 1;
	} finally {
		try {
			// Try to save results to DB even if it errored out above.
			await persist(scrapeResults);
			console.log('SAVED TO DB');
		} catch (err) {
			console.log('PERSISTENCE ERROR', err.message);
			process.exitCode = 1;
		}
	}

	console.timeEnd('total_duration');
};

main();
