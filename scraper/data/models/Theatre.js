const mongoose  = require('mongoose')
const Schema    = mongoose.Schema
const { urlValidator } = require('./validators');


const TheatreSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true,
        match: urlValidator
    }
})


module.exports = mongoose.model('Theatre', TheatreSchema);