const mongoose  = require('mongoose')
const Schema    = mongoose.Schema

const MetaSchema = new Schema({
    collectedAt: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Meta', MetaSchema);