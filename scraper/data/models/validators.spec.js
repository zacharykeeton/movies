const assert = require('assert');
const validators = require('./validators');

describe('The urlValidator', () => {

    it('should exist', () => {
        assert.ok(validators.urlValidator);
    });

    describe('should match "good" urls', () => {

        const testCases = [
            { input: 'https://www.santikos.com/image?url=http://sanviswebsvr.santikos.com/CDN/Image/Entity/FilmPosterGraphic/HO00005170&width=300&height=450' },
            { input: 'https://www.santikos.com/san-antonio/palladium-imax/film-info/the-intruder#showdetails' },
            { input: 'https://www.santikos.com/image?url=http://filmdb.santikos.com/FilmImages/40/1/74376/SHAZAM web.jpg&width=300&height=450' }  // in the wild (space in URL)
        ];

        for(let testCase of testCases) {
            it(`should match on ${testCase.input}.`, function () {
                assert.equal(validators.urlValidator.test(testCase.input), true);
            });
        }

    });

    describe('should NOT match "bad" urls', () => {

        const testCases = [
            { input: 'httpwww.example.com/' },
            { input: 'someotherstringaccidentallyassigned' }
        ];

        for(let testCase of testCases) {
            it(`should match on ${testCase.input}.`, function () {
                assert.equal(validators.urlValidator.test(testCase.input), false);
            });
        }

    });

});