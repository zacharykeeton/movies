/**
 * Used in our mongoose models.
 */

module.exports = {
     urlValidator: /^(http|https):\/\/[^"]+$/   // very permissive, even allows spaces in url (saw it once on a poster URL)
}