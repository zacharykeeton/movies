const mongoose  = require('mongoose')
const Schema    = mongoose.Schema
const { urlValidator } = require('./validators');

const ShowingSchema = new Schema({
    showtime: {
        type: Number,
        required: true
    },
    movieTitle: {
        type: String,
        required: true
    },
    theatreName: {
        type: String,
        required: true
    },
    moviePosterUrl: {
        type: String,
        required: true,
        match: urlValidator

    },
    movieDetailsUrl: {
        type: String,
        required: true,
        match: urlValidator
    }
})

module.exports = mongoose.model('Showing', ShowingSchema);