const mongoose  = require('mongoose')
const Schema    = mongoose.Schema

const CollectionErrorSchema = new Schema({
    message: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('CollectionError', CollectionErrorSchema);