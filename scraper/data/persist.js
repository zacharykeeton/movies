/**
 * Handles saves to DB via mongoose.
 * Using mongoose to handle vetting and
 * consistency assurance of data going
 * in to db.
 */

const mongoose = require("mongoose");
const Theater = require("./models/Theatre");
const Showing = require("./models/Showing");
const CollectionError = require("./models/CollectionError");
const Meta = require("./models/Meta");

async function connectToDbIfNotConnected() {
  // Get db url depending on mode.
  // see npm run start-dev
  let DB_URL = "mongodb://mongo-cluster-ip-service:27017/movies";
  // process.env.NODE_ENV === 'development'
  //     ? 'mongodb://localhost:27017/movies'
  //     : 'mongodb://mongo-cluster-ip-service:27017/movies'; // production

  // allows for mongo connection URL override via
  // environment variable.
  if (process.env.MOVIES_DB_URL) {
    DB_URL = process.env.MOVIES_DB_URL;
  }

  // if db connection status is 'connected' (1) or 'connecting' (2) do
  // nothing since we'll only need one connection.
  if ([1, 2].includes(mongoose.connection.readyState)) {
    return;
  }

  mongoose.set("useCreateIndex", true);
  return mongoose.connect(DB_URL, { useNewUrlParser: true });
}

async function saveToDb({ theatres, showings, meta, collectionErrors }) {
  console.log(`SAVING TO DB: ${showings.length} showings`);

  await connectToDbIfNotConnected();

  try {
    // Clear old data
    await mongoose.connection.db.dropDatabase();

    // Insert new data
    await Promise.all([
      Theater.insertMany(theatres),
      Showing.insertMany(showings),
      Meta.create(meta),
      CollectionError.insertMany(collectionErrors)
    ]);
  } catch (err) {
    throw err; // let client code handle error, but we want to do our .finally below:
  } finally {
    await mongoose.disconnect();
  }
}

module.exports = {
  connectToDbIfNotConnected,
  saveToDb
};
