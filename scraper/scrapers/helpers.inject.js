// injected by nightmare to be run in the browser
// NOTE: this is run in nightmare's execution context (i.e. as if 
// run on the page you're scraping).
// NOTE: always sync with helpers.js (lame way to test, need update.)

// HELPER FUNCS
window.dateToYYYY_MM_DD = function (date) {

    const pad = num => {
        const str = '0' + num;              // e.g. 5 => '05', 25 => '025'
        return str.substr(str.length - 2);  // return last two chars. eg. '05' => '05', '025' => '25'
    }

    const YYYY  = date.getFullYear();
    const MM    = pad(1 + date.getMonth());
    const DD    = pad(date.getDate()); 

    return `${YYYY}-${MM}-${DD}`;
};

window.timeStringToInt = function timeIntFromString(time) {
    
    if (time === 'Noon') {
        return 1200;
    }

    if (time === 'Midnight') {
        return 2400;
    }

    const parts = time.match(/(\d+):(\d+)\s?([ap])/i);
    if(!parts) return NaN;

    if (parts) {
        let hours = parseInt(parts[1]);
        const minutes = parseInt(parts[2]);
        const AM = parts[3].toLowerCase().startsWith('a');
        const PM = parts[3].toLowerCase().startsWith('p');
        if (AM && hours === 12)
            hours += 12;
        if (PM && hours < 12)
            hours += 12;
        return hours * 100 + minutes;
    }
    return NaN;
};

window.toTitleCase = function(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
};


// POCOS to help keep data structure integrity.
window.ScrapeResults = class ScrapeResults {
    constructor() {
        this.errors = [];
        this.showings = [];
    }
}

window.ScrapingError = class ScrapingError {
    constructor(message){
        this.message = message;
    }
}

window.Showing = class Showing {
    constructor(theatreName, movieTitle, posterUrl, detailsUrl, time){
        this.showtime           = time,
        this.movieTitle         = movieTitle,
        this.theatreName        = theatreName,
        this.moviePosterUrl     = posterUrl,
        this.movieDetailsUrl    = detailsUrl
    }
}

// execute the given scrape functions on the page and return the scraped results.
window.scrapeShowings = ({ getAllMovieNodes, theatreName, getMovieTitle, getPosterUrl, getDetailsUrl, getShowtimes }) => {
    const thisTheatreScrapeResults = new ScrapeResults();
    const movieNodes = getAllMovieNodes();

    for(let movieNode of movieNodes) {
        
        // movieTitle
        const movieTitle = getMovieTitle(movieNode);                
        if(!movieTitle) { continue; }  // if there is no title, assume there is no movie info here and just skip everything.
        
        // posterUrl
        const posterUrl = getPosterUrl(movieNode);
        
        // detailsUrl
        const detailsUrl = getDetailsUrl(movieNode);
        
        // showtimes 
        const showtimes = getShowtimes(movieNode);
        
        const thisMovieShowings = showtimes.map((twentyFourHourTimeAsInt) => 
            new Showing(theatreName, movieTitle, posterUrl, detailsUrl, twentyFourHourTimeAsInt));

        if(thisMovieShowings.length === 0) {
            thisTheatreScrapeResults.errors.push(new ScrapingError(`No showings gathered for ${theatreName}, ${movieTitle}`));
        } else {
            thisTheatreScrapeResults.showings.push(...thisMovieShowings);
        }
    }

    if(thisTheatreScrapeResults.showings.length === 0) {
        thisTheatreScrapeResults.errors.push(new ScrapingError(`No showings gathered for ${theatreName}`));
    }

    return thisTheatreScrapeResults;
}