/**
 * The scraper 'registry'.  This is the list used for the 
 * master scraping loop.  When you create a new scraper, be 
 * sure to add it into this array.
 */

const santikosScraper = require('./santikosScraper');
const drafthouseScraper = require('./alamoDrafthouseScraper');

module.exports = [santikosScraper, drafthouseScraper];