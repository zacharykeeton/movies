// NOTE: if you make changes here, make sure to sync helpers.inject.js

exports.dateToYYYY_MM_DD = function (date) {

    const pad = num => {
        const str = '0' + num;              // e.g. 5 => '05', 25 => '025'
        return str.substr(str.length - 2);  // return last two chars. eg. '05' => '05', '025' => '25'
    }

    const YYYY  = date.getFullYear();
    const MM    = pad(1 + date.getMonth());
    const DD    = pad(date.getDate()); 

    return `${YYYY}-${MM}-${DD}`;
}

exports.timeStringToInt = function timeIntFromString(time) {
    
    if (time === 'Noon') {
        return 1200;
    }

    if (time === 'Midnight') {
        return 2400;
    }

    const parts = time.match(/(\d+):(\d+)\s?([ap])/i);
    if(!parts) return NaN;

    if (parts) {
        let hours = parseInt(parts[1]);
        const minutes = parseInt(parts[2]);
        const AM = parts[3].toLowerCase().startsWith('a');
        const PM = parts[3].toLowerCase().startsWith('p');
        if (AM && hours === 12)
            hours += 12;
        if (PM && hours < 12)
            hours += 12;
        return hours * 100 + minutes;
    }
    return NaN;
}

exports.toTitleCase = function(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}
