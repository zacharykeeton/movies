/**
 * The actual scraping logic for santikos.com theatre sites.
 * Designed for extensibility, this file plays heavily with
 * Scraper.js, and helpers.inject.js.
 * To make a new scraper, use this as a template, create your
 * new scraping functions for the new website, and register
 * the new scraper file in 'index.js' to be added to the scrape
 * loop.
 */

const Scraper = require('./Scraper');

const label = 'Santikos';

const theatres = [
	{ name: 'Santikos CasaBlanca', url: 'https://www.santikos.com/san-antonio/casa-blanca' },
	{ name: 'Santikos Silverado', url: 'https://www.santikos.com/san-antonio/silverado-16' },
	{ name: 'Santikos Palladium', url: 'https://www.santikos.com/san-antonio/palladium-imax' },
	{ name: 'Santikos Bijou', url: 'https://www.santikos.com/san-antonio/bijou' },
	{ name: 'Santikos Embassy 14', url: 'https://www.santikos.com/san-antonio/embassy-14' },
	{ name: 'Santikos Mayan Palace', url: 'https://www.santikos.com/san-antonio/mayan-palace' },
	{ name: 'Santikos Northwest', url: 'https://www.santikos.com/san-antonio/northwest' },
	{ name: 'Santikos Rialto Brewhaus', url: 'https://www.santikos.com/san-antonio/rialto-brewhaus' },
	{ name: 'Santikos Cibolo', url: 'https://www.santikos.com/san-antonio/cibolo' },
];

// NOTE: this function is run in nightmare's execution context
// (i.e. as if run on the page you're scraping),
// so you cannot use most outside references.
// Also, these depend on functionality from 'helpers.inject.js'
// which will have been included on the page already.
const collector = function(theatreName) {
	const YYYY_MM_DD = dateToYYYY_MM_DD(new Date()); // dateToYYYY_MM_DD() from helpers.inject.js
	const base_url = 'https://www.santikos.com';

	const getAllMovieNodes = () => document.querySelectorAll(`li.filmItem[data-film-session*="${YYYY_MM_DD}"]`);

	const getMovieTitle = movieNode => {
		const titleNode = movieNode.querySelector('.filmItemContent .filmItemTitle > .filmItemInfoLink');
		if (!titleNode) {
			return null;
		} // if there is no title, assume there is no movie info here and just skip everything.
		return toTitleCase(titleNode.textContent);
	};

	const getPosterUrl = movieNode => {
		const imageNode = movieNode.querySelector('.filmItemImage > .filmItemInfoLink > img');
		const posterUrl = imageNode ? base_url + imageNode.getAttribute('src') : null;
		return posterUrl;
	};

	const getDetailsUrl = movieNode => {
		const detailsNode = movieNode.querySelector('.filmItemContent .filmItemTitle > .filmItemInfoLink');
		const detailsUrl = detailsNode ? base_url + detailsNode.getAttribute('href') : null;
		return detailsUrl;
	};

	const getShowtimes = movieNode => {
		const showtimes = [...movieNode.querySelectorAll(`[data-film-session='${YYYY_MM_DD}'] .filmSessionTime`)].map(
			function(timeNode) {
				const twentyFourHourTimeAsInt = timeStringToInt(timeNode.textContent); // from helpers.inject.js
				return twentyFourHourTimeAsInt;
			}
		);

		return showtimes;
	};

	return scrapeShowings({
		theatreName,
		getAllMovieNodes,
		getMovieTitle,
		getPosterUrl,
		getDetailsUrl,
		getShowtimes,
	});
};

const scraper = new Scraper(label, theatres, collector);
exports.scrapeAll = scraper.scrapeAllTheatres.bind(scraper);
exports.label = scraper.label;
