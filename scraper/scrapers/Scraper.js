/** Handles the actual calls to nightmare and packages results.
 *  Designed for extensibility, you use this scraper as an 
 *  executor only while writing the actual website-specific
 *  scraping logic in a 'collector'.
 *  See 'alamoDrafthouseScraper.js' and 'santikosScraper.js'
 *  for examples.
 */

const ScrapeResults = require('./ScrapeResults');

module.exports = class Scraper {

    constructor(label, theatres, collector){
        this.label = label;
        this.theatres = theatres;
        this.collector = collector;
    }

    async scrapeOneTheatre(nightmare, { name, url }) {
        return nightmare
          .goto(url)
          .inject('js', './scrapers/helpers.inject.js') // path relative to app.js
          .evaluate(this.collector, name);
      }

    async scrapeAllTheatres(nightmare) {

        const scraperResults = new ScrapeResults();

        for(let theatre of this.theatres) {
            console.log(`SCRAPING: ${theatre.name}`);
            const scrapeResults = await this.scrapeOneTheatre(nightmare, theatre);
            scraperResults.theatres.push(theatre);
            scraperResults.collectionErrors.push(...scrapeResults.errors);
            scraperResults.showings.push(...scrapeResults.showings);
        }
        
        return scraperResults;
    }
}