module.exports = class ScrapeResults {
    constructor() {
        this.theatres           = [],
        this.collectionErrors   = [];
        this.showings           = [];
        this.meta               = { collectedAt: null };
    }
}