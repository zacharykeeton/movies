const helpers = require('./helpers');
const assert = require('assert');

describe('timeStringToInt', function () {

    describe(`should return an int between 1 and 2400 given a time string in one of the supported formats.`, function () {

        const testCases = [
            // MORNING
            { input: '9:00a', expectedResult: 900 }, 
            { input: '9:00 AM', expectedResult: 900 }, 
            { input: '9:00am', expectedResult: 900 }, 
            { input: '9:00AM', expectedResult: 900 },
            
            { input: '9:45a', expectedResult: 945 }, 
            { input: '9:45 AM', expectedResult: 945 }, 
            { input: '9:45am', expectedResult: 945 }, 
            { input: '9:45AM', expectedResult: 945 },

            // NOON
            { input: '12:00p', expectedResult: 1200 }, 
            { input: '12:00 PM', expectedResult: 1200 }, 
            { input: 'Noon', expectedResult: 1200 }, 
            { input: '12:00pm', expectedResult: 1200 }, 
            { input: '12:00PM', expectedResult: 1200 }, 

            // NIGHT
            { input: '9:00p', expectedResult: 2100 }, 
            { input: '9:00 PM', expectedResult: 2100 }, 
            { input: '9:00pm', expectedResult: 2100 }, 
            { input: '9:00PM', expectedResult: 2100 },
            
            { input: '9:45p', expectedResult: 2145 }, 
            { input: '9:45 PM', expectedResult: 2145 }, 
            { input: '9:45pm', expectedResult: 2145 }, 
            { input: '9:45PM', expectedResult: 2145 },



            // MIDNIGHT
            { input: '12:00a', expectedResult: 2400 }, 
            { input: '12:00 AM', expectedResult: 2400 }, 
            { input: 'Midnight', expectedResult: 2400 }, 
            { input: '12:00am', expectedResult: 2400 }, 
            { input: '12:00AM', expectedResult: 2400 }, 

        ];

        for(let testCase of testCases) {
            it(`should return ${testCase.expectedResult} given the time string "${testCase.input}".`, function () {
                assert.equal(helpers.timeStringToInt(testCase.input), testCase.expectedResult);
            });
        }

    });

    describe(`should return NaN if the given time string is not in the supported format.`, function () {

        const testCases = [
            { input: '12:00' }, 
            { input: '12:00' }, 
        ];

        for(let testCase of testCases) {
            it(`should return NaN if the time string is "${testCase.input}"`, function () {
                assert.ok(isNaN(helpers.timeStringToInt(testCase.input)));
            });
        }

    });
});

describe('dateToYYYY_MM_DD', function () {

    describe(`should return an date string in the format of YYYY-MM-DD given a date object.`, function () {

        const testCases = [
            // MORNING
            { input: new Date(2019, 3, 23), expectedResult: '2019-04-23' },
            { input: new Date(2019, 3, 3), expectedResult: '2019-04-03' },
            { input: new Date(2019, 10, 30), expectedResult: '2019-11-30' },
            { input: new Date(2019, 10, 03), expectedResult: '2019-11-03' }

        ];

        for(let testCase of testCases) {
            it(`should return ${testCase.expectedResult} given the date "${testCase.input}".`, function () {
                assert.equal(helpers.dateToYYYY_MM_DD(testCase.input), testCase.expectedResult);
            });
        }

    });

    describe(`should throw if the given date is not a date object.`, function () {

        const testCases = [
            { input: 'text' },
            { input: 55 },
            { input: { month: 5, day: 20, year: 2019} }

        ];

        for(let testCase of testCases) {
            it(`should throw if the date input is "${testCase.input}"`, function () {
                assert.throws(() => helpers.dateToYYYY_MM_DD(testCase.input));
            });
        }

    });
});

describe('toTitleCase', function () {

    describe(`should return an 'title-cased' string given a string input.`, function () {

        const testCases = [
            { input: 'LITTLE', expectedResult: 'Little' },
            { input: 'CAPTAIN MARVEL [2D]', expectedResult: 'Captain Marvel [2d]' },
            { input: 'DUMBO (2019) [2D]', expectedResult: 'Dumbo (2019) [2d]' },
            { input: 'Shazam!', expectedResult: 'Shazam!' },
            { input: 'THE CURSE OF LA LLORONA', expectedResult: 'The Curse Of La Llorona' },
            { input: "Singin' in the Rain", expectedResult: "Singin' In The Rain" }

        ];

        for(let testCase of testCases) {
            it(`should return ${testCase.expectedResult} given the string "${testCase.input}".`, function () {
                assert.equal(helpers.toTitleCase(testCase.input), testCase.expectedResult);
            });
        }

    });

    describe(`should throw if the given input is not a string.`, function () {

        const testCases = [
            { input: [1,2,3] },
            { input: 55 },
            { input: { month: 5, day: 20, year: 2019} }

        ];

        for(let testCase of testCases) {
            it(`should throw if the input is ${testCase.input}`, function () {
                assert.throws(() => helpers.toTitleCase(testCase.input));
            });
        }

    });
});