/**
 * The actual scraping logic for drafthouse.com theatre sites.
 * Designed for extensibility, this file plays heavily with 
 * Scraper.js, and helpers.inject.js.
 * To make a new scraper, use this as a template, create your
 * new scraping functions for the new website, and register
 * the new scraper file in 'index.js' to be added to the scrape
 * loop.
 */

const Scraper = require('./Scraper');

const label = 'Alamo Drafthouse';

const theatres = [
	{ name: 'Drafthouse - Westlakes', url: 'https://drafthouse.com/san-antonio/theater/westlakes' },
	{ name: 'Drafthouse - Park North', url: 'https://drafthouse.com/san-antonio/theater/park-north' },
	{ name: 'Drafthouse - Stone Oak', url: 'https://drafthouse.com/san-antonio/theater/stone-oak' },
	{ name: 'Drafthouse - La Cantera', url: 'https://drafthouse.com/san-antonio/theater/la-cantera' },
];

// NOTE: this function is run in nightmare's execution context
// (i.e. as if run on the page you're scraping),
// so you cannot use most outside references.
// Also, these depend on functionality from 'helpers.inject.js'
// which will have been included on the page already.
const collector = function(theatreName) {
	const getAllMovieNodes = () => document.querySelectorAll('div.Showtimes-panel div.row');

	const getMovieTitle = movieNode => {
		const titleNode = movieNode.querySelector('h3.Showtimes-title a');
		if (!titleNode) {
			return null;
		} // if there is no title, assume there is no movie info here and just skip everything.
		return toTitleCase(titleNode.textContent);
	};

	const getPosterUrl = movieNode => {
		return 'https://drafthouse.com/assets/img/logo-condensed-black.png'; // hardcoded since posters not on this page.
	};

	const getDetailsUrl = movieNode => {
		const titleNode = movieNode.querySelector('h3.Showtimes-title a');
		return titleNode.getAttribute('href');
	};

	const getShowtimes = movieNode => {
		const showtimes = [...movieNode.querySelectorAll('button.Showtimes-time')].map(function(timeNode) {
			const twentyFourHourTimeAsInt = timeStringToInt(timeNode.textContent); // from helpers.inject.js
			return twentyFourHourTimeAsInt;
		});

		return showtimes;
	};

	return scrapeShowings({
		theatreName,
		getAllMovieNodes,
		getMovieTitle,
		getPosterUrl,
		getDetailsUrl,
		getShowtimes,
	});
};

const scraper = new Scraper(label, theatres, collector);
exports.scrapeAll = scraper.scrapeAllTheatres.bind(scraper);
exports.label = scraper.label;
