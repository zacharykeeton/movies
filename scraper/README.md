# SCRAPER

The scraper runs in a docker container with a cron job setup for 2:30am each day see `crontab`. It loops through all known scrapers (registered in `./scrapers/index.js`) and saves the data to the mongo container (which needs to be up before the scraper can run).

As of May 20, 2019, you can see it live at http://zacharykeeton.com/apps/movies/

## Development

To run in a dev environment, start a mongo container on localhost:27017.
```bash
docker run -p27017:27017 -d --name dev-mongo mongo
```

Then, you can run the scraper at will with `npm run start-dev`.

## Creating new scrapers

The Scraper was built with extensibility in mind. For most movie theatre websites, you can just copy one of the existing scrapiers (e.g. `./scrapers/santikosScraper.js`) and re-implement the functions you see there.  When you are finished, you must 'register' your new scraper in `./scrapers/index.js` for it to be executed with all the others.


## Build & Push
Once you are ready to deploy, you can go to `./scraper/` and do `npm run build-docker-push` (make sure you are on the correct branch). This will build the scraper image and push it to docker hub at `:latest`. See `package.json` for details.

