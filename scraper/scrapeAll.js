const Nightmare = require('nightmare');
const scrapers = require('./scrapers');
const ScrapeResults = require('./scrapers/ScrapeResults');

module.exports = async function() {    
    // open the nightmare window (headless).
    // To view during development, instead call 
    // Nightmare({ show: true, openDevTools: true })
	const nightmare = Nightmare();

    // The master data accumulation obj.
	const allResults = new ScrapeResults();

    // Loop through all scrapers and accumulate results.
	for (scraper of scrapers) {
		try {
            // Scrape
            // need to pass the single nightmare reference down to scrapers,
            // so we can close it's electron instance at the end.
			const scraperResult = await scraper.scrapeAll(nightmare);

            // Gather results
			allResults.theatres.push(...scraperResult.theatres);
			allResults.collectionErrors.push(...scraperResult.collectionErrors);
			allResults.showings.push(...scraperResult.showings);
		} catch (err) {
            console.log(err);
            
            // Accumulate errors in the errors array for push up to ui.
			allResults.collectionErrors.push({ message: `${scraper.label} scraping failed` });
		}
	}

    // close the nightmare window
	await nightmare.end();
    
    // sort theatres by name ascending
    // before saving to db better than 
    // sorting in-query.
	allResults.theatres.sort((a, b) => {
        return a.name <= b.name ? -1 : 1;
	});
    
    // sort showings by showtime (integer) ascending
    // before saving to db better than 
    // sorting in-query.
	allResults.showings.sort((a, b) => {
        return a.showtime <= b.showtime ? -1 : 1;
	});
    
    // add collection timestamp
    allResults.meta.collectedAt = new Date();
    
    return allResults;
};
